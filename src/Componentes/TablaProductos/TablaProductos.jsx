import React from "react";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { Row, Container } from "reactstrap";

export const tabla = (carrito) => {
  const fila = () => {
    carrito.map((producto) => {
      return (
        <TableRow key={producto.id}>
          <TableCell>{producto.nombre}</TableCell>
          <TableCell>{producto.precio}</TableCell>
        </TableRow>
      );
    });
  };

  const total = 0;
  return (
    <Container>
      <Row>
        <TableContainer>
          <Table stickyHeader>
            <TableHead>
              <TableRow>
                <TableCell>Nombre</TableCell>
                <TableCell>Precio Unitario</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{() => fila()}</TableBody>
          </Table>
        </TableContainer>
      </Row>
      {/* <Row>
        <h5>
          Total: $
          {carrito.map((producto) => {
            return (total = total + producto.precio);
          })}
        </h5>
      </Row> */}
    </Container>
  );
};
