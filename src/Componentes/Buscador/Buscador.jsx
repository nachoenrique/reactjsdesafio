import React, { useState, useEffect } from "react";
import { Search, Header, Icon } from "semantic-ui-react";

export const buscador = (onResultSelect, handleSearchChange, autocomplete) => {

  const resultRenderer = ({ nombre, precio }) => (
    <Header size="tiny">
      <Icon className="iconAutocomplete" name="angle right" />
      <Header.Content>{nombre + " - $" + precio}</Header.Content>
    </Header>
  );
  
  return (
    <Search
      fluid
      input={{ icon: "search", iconPosition: "left" }}
      placeholder="Buscar producto"
      onResultSelect={onResultSelect}
      onSearchChange={handleSearchChange}
      results={autocomplete.results}
      value={autocomplete.value}
      resultRenderer={resultRenderer}
      noResultsMessage="No se encontro ningun producto"
    />
  );
};
