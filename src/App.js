import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import { Col, Row } from "react-bootstrap";
import { buscador } from "./Componentes/Buscador/Buscador";
import _ from "lodash";
import { productos } from "./Datos/productos";
import { tabla } from "./Componentes/TablaProductos/TablaProductos";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

function App() {
  const estadoInicial = {
    productos: [],
    autocomplete: {
      isLoading: false,
      results: [],
      value: "",
    },
    carrito: [],
  };
  const [estado, setEstado] = useState(estadoInicial);

  /**Apenas se ejecute la @function App, setea el valor de productos del estado  */
  useEffect(() => {
    setEstado({ ...estado, productos: productos });
  }, []);

  // const agregarProducto = ({ result }) => {
  //   const carritoVar = estado.carrito;
  //   const estaElProduct = false
  //   if (carritoVar.length === 0) {
  //     result.cantidad = 1;
  //     carritoVar.push(result);
  //     setEstado({
  //       ...estado,
  //       carrito: carritoVar,
  //     });
  //   } else {
  //     for (let i = 0; i < carritoVar.length; i++) {
  //       if (carritoVar[i].id === result.id) {
  //         estaElProduct = true
  //         carritoVar[i].cantidad = carritoVar[i].cantidad + 1;
  //         setEstado({
  //           ...estado,
  //           carrito: carritoVar,
  //         });
  //         break;
  //         i--;
  //       }
  //       if (estaElProduct === false )
  //     }
  //   }
  // };

  

  /** Esta funcion se ejecuta cuando selecciono un producto del buscador. Al seleccionar un producto se setea el valor de value y de productosSeleccionado */
  const handleResultSelect = (e, { result }) => {
    setEstado({
      ...estado,
      autocomplete: { ...estado.autocomplete, value: "" },
    });

    const productosSelect = estado.carrito;
    productosSelect.push(result);
    setEstado({
      ...estado,
      carrito: productosSelect,
    });
  };

  /** Esta funcion se ejecuta cuando se produce un evento en el buscador  */
  const handleSearchChange = (value) => {
    console.log("State:", estado);
    setEstado({
      ...estado,
      autocomplete: { ...estado.autocomplete, isLoading: true, value },
    });

    //Si value no tiene nada, se setea autocomplete a su valor original sino setea result con los resultados de la busqueda
    setTimeout(() => {
      if (estado.autocomplete.value === "")
        return setEstado({
          ...estado,
          autocomplete: {
            isLoading: false,
            results: [],
            value: "",
          },
        });

      const re = new RegExp(_.escapeRegExp(value), "i");
      const isMatch = (result) => re.test(result.nombre);

      setEstado({
        ...estado,
        autocomplete: {
          ...estado.autocomplete,
          value,
          isLoading: false,
          results: _.filter(estado.productos, isMatch),
        },
      });
    }, 200);
  };

  return (
    <div className="App">
      <Row className="App-header">
        <h1>¡Bienvenidos!</h1>
      </Row>

      <Row>
        <Col xs={12} sm={12} md={5} lg={5} xl={5}>
          <Row
            style={{
              justifyContent: "center",
              textAlign: "center",
              alignContent: "center",
              width: "100%",
            }}
          >
            <h3>Buscar tus productos</h3>
          </Row>
          <Row
            style={{
              justifyContent: "center",
              textAlign: "center",
              alignContent: "center",
              width: "100%",
            }}
          >
            {buscador(
              (e, result) => handleResultSelect(e, result),
              (e, { value }) => handleSearchChange(value),
              estado.autocomplete
            )}
          </Row>
        </Col>
        <Col
          xs={12}
          sm={12}
          md={7}
          lg={7}
          xl={7}
          style={{
            justifyContent: "center",
            textAlign: "center",
            alignContent: "center",
          }}
        >
          <Row
            style={{
              justifyContent: "center",
              textAlign: "center",
              alignContent: "center",
              width: "100%",
            }}
          >
            <h3>Carrito</h3>
          </Row>
          <Row style={{ width: "100%" }}>{tabla(estado.carrito)}</Row>
        </Col>
      </Row>
    </div>
  );
}

export default App;
